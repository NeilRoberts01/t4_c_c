#include <Servo.h>

#include "BLE_RX.hpp"

#include "Wheel.hpp"                                        
#include "Drive_Subsystem.hpp"

#ifndef _SKETCH_INO
#define _SKETCH_INO

#define SERIAL_SPEED 9600
#define SENSOR_PRESSED 0
#define TIMEOUT_WINDOW 1500
#define JOYSTICK_DAMPING 10
#define JOYSTICK_DEADZONE 30                                // only accept joystick values greater than this number
#define JOYSTICK_MEDIAN 128                           

// Define Drive Subsystem
Drive_Subsystem *drive_subsystem;

// Define Wheel Motors (Servos)
Wheel *drive_l;
Wheel *drive_r;

// Define wheel control pins
int dl_control = 10;
int dr_control = 11;

// Define wheel encoder pins
int dl_feedback = 9;
int dr_feedback = 2;

// Define BT pins                                             (Serial1 pins)
int bt_rx = 18;
int bt_tx = 19;

// Define Door pins                                          180 - 0 deg toggle
int door_pin = 13;
bool curr_door_state = false;
bool prev_door_state = false;
float door_open_angle = 180;                              // PLACEHOLDER VALUE
float door_close_angle = 0;                               // PLACEHOLDER VALUE

Servo door_servo;


// Define Pickup Motor                                     
int pickup_motor_enable = 12;

// Define current sensor                            return analogue voltage    if voltage too high, emergency stop
int current_sensor = A0;
float max_Voltage = 1;


// Global Variables
int left_joy = 0;
int right_joy = 0;
float l_wheel_rpm = 0;
float r_wheel_rpm = 0;

void setup() {
  
  BLE_MEGA_init();
  
  drive_l = new Wheel(dl_control, dl_feedback);
  drive_r = new Wheel(dr_control, dr_feedback);

  drive_subsystem = new Drive_Subsystem(drive_l, drive_r);
  
  door_servo.attach(door_pin);
  door_servo.write(door_close_angle);

  pinMode(pickup_motor_enable, OUTPUT);
  digitalWrite(pickup_motor_enable, HIGH);

  pinMode(current_sensor, INPUT_PULLUP);

  pinMode(dl_feedback, INPUT);
  pinMode(dr_feedback, INPUT);

  attachInterrupt(digitalPinToInterrupt(dl_feedback), updateLeftEncoder, RISING);
  attachInterrupt(digitalPinToInterrupt(dr_feedback), updateRightEncoder, RISING);
}

void loop() {

  // if voltage reading too high, emergency stop
  if (analogRead(current_sensor) > max_Voltage) {
      emergencyShutdown();
  }

  //TIMEOUT
  connection_timeout();

  if (connection()) {
      
        if (emergency_stop()) {
            emergencyShutdown();
        }

        curr_door_state = open_close();

        if (curr_door_state == false) {

            left_joy = left_joystick();
            right_joy = right_joystick();
            
            // process input
            if (left_joy > JOYSTICK_MEDIAN - JOYSTICK_DEADZONE && 
                left_joy < JOYSTICK_MEDIAN + JOYSTICK_DEADZONE)
                left_joy = JOYSTICK_MEDIAN;

            if (right_joy > JOYSTICK_MEDIAN - JOYSTICK_DEADZONE &&
                right_joy < JOYSTICK_MEDIAN + JOYSTICK_DEADZONE)
                right_joy = JOYSTICK_MEDIAN;

            if (abs(left_joy - right_joy) <= JOYSTICK_DAMPING)
                right_joy = left_joy;

            // execute
            drive_subsystem->drive(left_joy, right_joy);

            // get wheel speed
            rpm_l( drive_l->pollLeftCurrentSpeed() );            // TODO: wants byte, poll gives float
            rpm_r( drive_r->pollRightCurrentSpeed() );
        }                                
  }

  if (curr_door_state == true && prev_door_state == false) {
      door_servo.write(door_open_angle);
  }
  else if (curr_door_state == false && prev_door_state == true) {
      door_servo.write(door_close_angle);
  }
  prev_door_state = curr_door_state;

}

void emergencyShutdown() {

    digitalWrite(pickup_motor_enable, LOW);             // turn off pickup motor
    door_servo.write(door_close_angle);                 // send door close signal

    while(true) {                                       // :)
    /*_.-'''''-._
    .'  _     _  '.
   /   (_)   (_)   \
  |  ,           ,  |
  |  \`.       .`/  |
   \  '.`'""'"`.'  /
    '.  `'---'`  .'
      '-._____.-' */
    }
}

#endif
