// Drive Subsystem
/*
 * Is used for driving the chassis
 * 
 * Defines:
 *  - enum Direction
 *  - Drive_Subsystem
 *    - void reset
 *    - void drive
 */

#include "Wheel.hpp"

#ifndef _DRIVE_SUBSYSTEM_HPP
#define _DRIVE_SUBSYSTEM_HPP
#define SPEED_TOLERANCE_CHANGE 0

enum Direction { left, right, forwards, backwards, turn_left, turn_right, drive_stop };

class Drive_Subsystem {
private:
	Wheel *_dl; //drive_left_top;
	Wheel *_dr; //drive_left_bottom;

  int ghost_angle;
  int dl_angle;
  int dr_angle;
  
  int dl_speed;
  int dr_speed;

  int angleIncrement = 47;
  int start_ghost_angle = -200;
  int incrementSpeed = 15;

public:
	Drive_Subsystem(Wheel *dl, Wheel *dr) {
		_dl = dl;
		_dr = dr;

    ghost_angle = start_ghost_angle; // Give real-time wheels a head start to speed up.

		reset();
	}

  // reset
  /*
   * Input:
   *  - None
   * Output:
   *  - Robot becomes stationary
   */
	void
	reset() {
		// Make robot stationary
    Serial.print("Drive_Subsystem - Reseting the wheels\n");
    Serial.print("Drive_Subsystem - Attempting dl\n");
		_dl->reset();
    Serial.print("Drive_Subsystem - Finished dl\n");
    Serial.print("Drive_Subsystem - Attempting dr\n");
		_dr->reset();
    Serial.print("Drive_Subsystem - Finished dr\n");
    Serial.print("Drive_Subsystem - Reset all wheels\n");
	}

  // drive
  /*
   * Input:
   *  - Direction you wish the robot to move in
   *  - Speed of the servos.
   * Output:
   *  - Visible movement of robot
   * Note:
   *  - Cascade fall on affect has been used when programming the switch case statement.
   *    Please note the breaks within the statement.
   */
	void
	drive(int dl_Speed, int dr_Speed) {           // raw joystick values
    bool debug = true;
    
    _dl->setSpeed(dl_Speed);
    _dr->setSpeed(dr_Speed);
    
		return; // distance done.	
	}
};

#endif // Drive_Subsystem.hpp
